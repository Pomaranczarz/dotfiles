#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias grep='grep --color=auto'

PS1='[\u@\h \W]\$ '
#eval "$(starship init bash)"

#alias ls='eza --icons=always --hyperlink'
#alias ll='eza -lh --icons=always --hyperlink'
#alias code='vscodium'
