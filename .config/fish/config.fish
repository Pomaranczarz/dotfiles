source /usr/share/cachyos-fish-config/cachyos-config.fish

# overwrite greeting
# potentially disabling fastfetch
#function fish_greeting
#    # smth smth
#end

function fish_greeting

end

export BROWSER firefox

zoxide init fish | source
starship init fish | source

alias l. eza\\\ -a\\\ \\\|\\\ grep\\\ -e\\\ \\\'^\\\\.\\\'
alias ll 'eza -lh --color=always --group-directories-first --icons'
alias ls 'eza --color=always --group-directories-first --icons'
alias la 'eza -ah --color=always --group-directories-first --icons'
alias lt 'eza -aT --color=always --group-directories-first --icons'

fish_add_path '/home/pomaranczarz/.cargo/bin'

alias cd z
