#!/usr/bin/bash

# check if yay is installed
if ! command -v yay &> /dev/null; then
    echo "yay not found. Exiting..."
    exit 1
fi

# read common packages names from 'common_packages.txt'
cat common_packages.txt | xargs yay -S --needed --noconfirm

# ask user if 1. sway or 2. hyprland
printf "Choose window manager:\n1. sway\n2. hyprland\n"
read -r choice

case $choice in
    1)
        # read sway packages names from 'sway_packages.txt'
        cat sway_packages.txt | xargs yay -S --needed --noconfirm
        ;;
    2)
        # read hyprland packages names from 'hyprland_packages.txt'
        cat hyprland_packages.txt | xargs yay -S --needed --noconfirm
        ;;
    *)
        echo "Invalid choice. Exiting..."
        exit 1
        ;;
esac
