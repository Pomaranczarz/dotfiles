# Summary
This repo contains dotfiles for my most current setup. It uses catppuccin theme for every app possible. Directory structure is adjusted for the use of GNU stow.

# Installation
For quick installation there's a [`install_packages.bash`](install_packages.bash) script that can be used to install both manager-agnostic packages and manager-specific ones (It provides a choice between sway and hyprland):
- [`common_packages.txt`](common_packages.txt) - common packages for both managers
- [`sway_packages.txt`](sway_packages.txt) - packages for [sway](https://github.com/swaywm/sway)
- [`hyprland_packages.txt`](hyprland_packages.txt) - packages for [hyprland](https://github.com/hyprwm/hyprland)

To install the configuration files, run `stow .` in the repo root.

# Showcase
![desktop](./showcase/desktop.png)

![thunar](./showcase/thunar.png)

![spotify](./showcase/spotify.png)